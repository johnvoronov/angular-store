import { Component, Inject, OnInit } from '@angular/core';
import {ProductService} from '../models/product.service';
import {CartService} from '../models/cart.service';
import {ActivatedRoute} from '@angular/router';
import {environment} from '../../environments/environment';
import { Product } from '../models/product.model';
import {Topping} from '../models/topping.model';
import {ToppingService} from '../models/topping.service';
import {WrapperComponent} from '@shared/layouts/wrapper/wrapper.component';
import * as $ from 'jquery'

@Component({
  moduleId: module.id,
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  providers: [ ProductService, ToppingService, CartService ]
})

export class CartComponent implements OnInit {
  public cart: Product[]
  public products: Product[]
  public productId: string
  public toppings: Topping[]
  public defaultImage: string
  public totalCart: number
  public apiUrl: string
  public error: string

  constructor(private productService: ProductService,
              private toppingService: ToppingService,
              private cartService: CartService,
              private route: ActivatedRoute,
              @Inject(WrapperComponent) private wrapperComponent: WrapperComponent) {
    this.route.params.subscribe(res => this.productId = res.product_id)
    this.wrapperComponent.isRootRoute = false
    this.wrapperComponent.isMap = false
    this.defaultImage = environment.defaultImage
    this.apiUrl = environment.apiUrl
    this.calculateCart()
    this.getCart()
  }

  ngOnInit() {

  }

  getCart() {
    this.cartService.getCart()
      .subscribe(data => {
        this.products = data['response']
        console.log(data['response'])
      }, error => {
        this.error = error;
      })
  }

  addProductToCart(product: Product) {
    this.cartService.addProductToCart(product)
      .subscribe(data => {
        this.products = data['response']
        this.calculateCart()
      }, error => {
        this.error = error;
      })
  }

  addProductWithComponentsToCart(product: Product) {
    this.cartService.addProductWithComponentsToCart(product, product.components.list)
      .subscribe(data => {
        this.products = data['response']
        this.calculateCart()
      }, error => {
        this.error = error;
      })
  }

  deleteProductFromCart(product: Product) {
    this.cartService.deleteProductFromCart(product._id)
      .subscribe(data => {
        this.products = data['response']
        this.calculateCart()
      }, error => {
        this.error = error;
      })
  }

  deleteProductWithComponentsFromCart(product: Product) {
    this.cartService.deleteProductWithComponentsFromCart(product._id, product.components.list)
      .subscribe(data => {
        this.products = data['response']
        console.log(data['response'])
        this.calculateCart()
      }, error => {
        this.error = error;
      })
  }

  deleteProductFullFromCart(product: Product) {
    this.cartService.deleteProductFullFromCart(product._id)
      .subscribe(data => {
        this.products = data['response']
        this.calculateCart()
      }, error => {
        this.error = error;
      })
  }

  deleteProductWithComponentsFullFromCart(product: Product) {
    this.cartService.deleteProductWithComponentsFullFromCart(product._id, product.components.list)
      .subscribe(data => {
        this.products = data['response']
        this.calculateCart()
      }, error => {
        this.error = error;
      })
  }

  addToppingToProduct(topping: Topping, productId: string) {
    this.cartService.addToppingToProduct(topping, productId)
      .subscribe(data => {
        this.products = data['response']
        this.calculateCart()
      }, error => {
        this.error = error;
      })
  }

  deleteToppingFromProduct(productId: string, toppingId: string) {
    this.cartService.deleteToppingFromProduct(productId, toppingId)
      .subscribe(data => {
        this.products = data['response']
        this.calculateCart()
      }, error => {
        this.error = error;
      })
  }

  deleteToppingFullFromProduct(productId: string, toppingId: string) {
    this.cartService.deleteToppingFullFromProduct(productId, toppingId)
      .subscribe(data => {
        this.products = data['response']
        this.calculateCart()
      }, error => {
        this.error = error;
      })
  }

  calculateCart() {
    this.cartService.calculateCart()
      .subscribe(data => {
        const header = $('#cart')
        if (header.hasClass('allow')) {
          header.removeClass('bounce-in-down')
          header.addClass('bounce')
          setTimeout(() => {
            header.removeClass('bounce')
          }, 500)
        } else {
          header.addClass('allow')
        }

        this.wrapperComponent.totalCart = data['response']
        this.totalCart = this.wrapperComponent.totalCart
      }, error => {
        this.error = error;
      })
  }
}
