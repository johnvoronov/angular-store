import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { CartRoutes } from './cart.routing'
import { CartComponent } from './cart.component'

@NgModule({
  imports: [
    CommonModule,
    CartRoutes
  ],
  declarations: [CartComponent]
})
export class CartModule { }
