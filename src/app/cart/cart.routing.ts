import { CartComponent } from './cart.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '', component: CartComponent,
    data: {
      meta: {
        title: 'Корзина',
        override: true,
        description: 'Корзина'
      }
    },
  },
];

export const CartRoutes = RouterModule.forChild(routes);
