import { ProductComponent } from './product.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '', component: ProductComponent,
    data: {
      meta: {
        title: 'Название продукта',
        override: true,
        description: 'Описание продукта'
      }
    },
  },
];

export const ProductRoutes = RouterModule.forChild(routes);
