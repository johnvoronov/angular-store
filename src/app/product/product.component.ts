import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { ProductService } from '../models/product.service'
import { ToppingService } from '../models/topping.service'
import { CartService } from '../models/cart.service';
import { ComponentsService } from '../models/components.service';
import { Product } from '../models/product.model'
import { Topping } from '../models/topping.model'
import { Components } from '../models/components.model'
import { environment } from '../../environments/environment'
import {WrapperComponent} from '@shared/layouts/wrapper/wrapper.component';
import {MetaService} from '@ngx-meta/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

const uuidv4 = require('uuid/v4');

import * as $ from 'jquery'
declare var UIkit: any


@Component({
  moduleId: module.id,
  selector: 'app-product',
  templateUrl: './product.component.html'
})

export class ProductComponent implements OnInit{
  public product: Product
  public popularProducts: Product[]
  public productId: string
  public toppings: Topping[]
  public isToppingsIncluded: boolean
  public defaultImage: string
  public apiUrl: string
  public totalCart: number
  public error: string
  public cart: Product[]
  public disabled: string

  constructor(private readonly meta: MetaService,
              private productService: ProductService,
              private toppingService: ToppingService,
              private cartService: CartService,
              private componentsService: ComponentsService,
              private route: ActivatedRoute,
              @Inject(WrapperComponent) private wrapperComponent: WrapperComponent) {
    this.route.params.subscribe(res => this.productId = res.product_id)
    this.wrapperComponent.isRootRoute = false
    this.wrapperComponent.isMap = false
    this.defaultImage = environment.defaultImage
    this.apiUrl = environment.apiUrl
    this.isToppingsIncluded = false
  }

  ngOnInit() {
    this.getCart()
    this.getPopularProducts()
  }

  getProduct(productId: string) {
    this.productService.getProduct(productId)
      .subscribe(data => {
        data['response'].count = 0
        this.product = data['response']

        if (this.product.toppings !== undefined) {
          this.isToppingsIncluded = true
        }

        this.meta.setTitle(data['response'].title)
        this.meta.setTag('description', data['response'].title)
        this.meta.setTag('og:title', data['response'].title)
        this.meta.setTag('og:description', data['response'].composition)
        this.meta.setTag('og:image', this.apiUrl + data['response'].images[7].src)

        if (this.cart !== null && this.cart !== undefined && this.cart.length > 0) {
          this.cart.map(product => {
            if (product._id === productId) {
              this.product = product
            }
          })
        }
      }, error => {
        this.error = error;
      })
  }

  openToppingsModal() {
      UIkit.modal('#item-modal-id').show();
  }

  getPopularProducts() {
      this.productService.getPopularProducts()
          .subscribe(data => {
              this.popularProducts = data['response']
          }, error => {
              this.error = error
          })
  }

  addProductToCart(product: Product) {
    this.cartService.addProductToCart(product)
      .subscribe(data => {
        this.product.count += 1
        // this.wrapperComponent.notify(this.apiUrl + product.images[1].src, '<b>' + product.title + '</b> успешно добавлен(а) в корзину')
        this.calculateCart()
      }, error => {
        this.error = error;
      })
  }

  deleteProductFromCart(productId: string) {
      this.cartService.deleteProductFromCart(productId)
          .subscribe(data => {
              this.product.count -= 1
              this.calculateCart()
          }, error => {
              this.error = error;
          })
  }

  getCart() {
    this.cartService.getCart()
      .subscribe(data => {
        this.cart = data['response']
        this.getProduct(this.productId)
      }, error => {
        this.error = error;
      })
  }

  calculateCart() {
    this.cartService.calculateCart()
      .subscribe(data => {
        const header = $('#cart')
        if (header.hasClass('allow')) {
          header.removeClass('bounce-in-down')
          header.addClass('bounce')
          setTimeout(() => {
            header.removeClass('bounce')
          }, 500)
        } else {
          header.addClass('allow')
        }

        this.wrapperComponent.totalCart = data['response']
      }, error => {
          this.error = error;
      })
  }
}
