import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductRoutes } from './product.routing';
import { ProductComponent } from './product.component';
import {ToppingsModalModule} from '@shared/layouts/modal/toppings/toppings-modal.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ProductRoutes,
    ToppingsModalModule
  ],
  declarations: [ ProductComponent ]
})
export class ProductModule { }