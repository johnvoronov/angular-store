import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { ProductService } from '../models/product.service'
import { ToppingService } from '../models/topping.service'
import { CartService } from '../models/cart.service';
import { ComponentsService } from '../models/components.service';
import { Product } from '../models/product.model'
import { Topping } from '../models/topping.model'
import { Components } from '../models/components.model'
import { environment } from '../../environments/environment'
import {WrapperComponent} from '@shared/layouts/wrapper/wrapper.component';
import {MetaService} from '@ngx-meta/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

const uuidv4 = require('uuid/v4');

import * as $ from 'jquery'
declare var UIkit: any


@Component({
  moduleId: module.id,
  selector: 'app-product',
  templateUrl: './components.component.html'
})

export class ComponentsComponent implements OnInit{
  public product: Product
  public products: Product[]
  public popularProducts: Product[]
  public productId: string
  public components: Components[]
  public toppings: Topping[]
  public isToppingsIncluded: boolean
  public isSimpleProduct: boolean
  public isComponentsIncluded: boolean
  public isHideGenerator: boolean
  public defaultImage: string
  public apiUrl: string
  public totalCart: number
  public cart: Product[]
  public disabled: string
  public componentsForm: FormGroup
  public error: string

  constructor(private readonly meta: MetaService,
              private productService: ProductService,
              private toppingService: ToppingService,
              private cartService: CartService,
              private componentsService: ComponentsService,
              private route: ActivatedRoute,
              @Inject(WrapperComponent) private wrapperComponent: WrapperComponent) {
    this.route.params.subscribe(res => this.productId = res.product_id)
    this.wrapperComponent.isRootRoute = false
    this.wrapperComponent.isMap = false
    this.defaultImage = environment.defaultImage
    this.apiUrl = environment.apiUrl
    this.isToppingsIncluded = false
    this.isComponentsIncluded = false
    this.isSimpleProduct = false
    this.isHideGenerator = false
    this.products = []
  }

  ngOnInit() {
    this.getCart()
    this.getPopularProducts()
    this.onChanges()
  }

  onChanges(): void {
    // Change order type
    // console.log(this.componentsForm)
    /*this.componentsForm.get('deliveryType').valueChanges.subscribe(value => {
      if (value === this.deliveryType[0]) {
        this.switchOrderType(true, false, false)
      } else if (value === this.deliveryType[1]) {
        this.switchOrderType(false, true, false)
      } else if (value === this.deliveryType[2]) {
        this.switchOrderType(false, false, true)
      }
    })*/
  }

  getProduct(productId: string) {
    this.productService.getProduct(productId)
      .subscribe(data => {
        data['response'].count = 0
        this.product = data['response']

        if (this.product.toppings !== undefined) {
          this.isToppingsIncluded = true
        }

        this.getComponentsByCategory(this.product.components._id)

        this.meta.setTitle(data['response'].title)
        this.meta.setTag('description', data['response'].title)
        this.meta.setTag('og:title', data['response'].title)
        this.meta.setTag('og:description', data['response'].composition)
        this.meta.setTag('og:image', this.apiUrl + data['response'].images[7].src)

        if (this.cart !== null && this.cart !== undefined && this.cart.length > 0) {
          this.cart.map(product => {
            if (product._id === productId) {
              this.product = product
            }
          })
        }
      }, error => {
        this.error = error;
      })
  }

  getProducts(productId: string) {
    this.products = []
    this.cart.map(product => {
      if (product._id === productId) {
        this.products.push(product)
        // console.log(this.products)
      }
    })

    if (this.products.length === 0) {
      this.isHideGenerator = false
    }
  }

  openToppingsModal() {
      UIkit.modal('#item-modal-id').show();
  }

  getComponentsByCategory(categoryId: string) {
    this.componentsService.getComponentsByCategory(categoryId)
      .subscribe(data => {
        this.components = data['response']

        let group: any = {};

        this.components.forEach((component, index) => {
          // console.log(component.title)
          group[component._id] = new FormControl(component.products[0].title, Validators.required)
        })

        this.componentsForm = new FormGroup(group)
        // console.log(this.componentsForm.controls)
        this.isComponentsIncluded = true
      }, error => {
        this.error = error
      })
  }

  getPopularProducts() {
      this.productService.getPopularProducts()
          .subscribe(data => {
              this.popularProducts = data['response']
          }, error => {
              this.error = error
          })
  }

  addProductWithComponentsToCart(product: Product, components: any) {
    let componentsResult = []

    for (let component in components) {
      componentsResult.push(components[component])
    }

    this.cartService.addProductWithComponentsToCart(product, componentsResult.join(', '))
      .subscribe(data => {
        // console.log(this.product)
        this.isHideGenerator = true
        // this.wrapperComponent.notify(this.apiUrl + product.images[1].src, '<b>' + product.title + '</b> успешно добавлен(а) в корзину')
        this.calculateCart()
      }, error => {
        this.error = error;
      })
  }

  addMore() {
    this.isHideGenerator = false
  }

  deleteProductFromCart(productId: string) {
      this.cartService.deleteProductFromCart(productId)
          .subscribe(data => {
              this.product.count -= 1
              this.calculateCart()
          }, error => {
              this.error = error;
          })
  }

  getCart() {
    this.cartService.getCart()
      .subscribe(data => {
        this.cart = data['response']
        this.getProduct(this.productId)
      }, error => {
        this.error = error;
      })
  }

  calculateCart() {
    this.cartService.calculateCart()
      .subscribe(data => {
        const header = $('#cart')
        if (header.hasClass('allow')) {
          header.removeClass('bounce-in-down')
          header.addClass('bounce')
          setTimeout(() => {
            header.removeClass('bounce')
          }, 500)
        } else {
          header.addClass('allow')
        }

        this.wrapperComponent.totalCart = data['response']
      }, error => {
          this.error = error;
      })
  }
}
