import { ComponentsComponent } from './components.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '', component: ComponentsComponent,
    data: {
      meta: {
        title: 'Название продукта',
        override: true,
        description: 'Описание продукта'
      }
    },
  },
];

export const ComponentsRoutes = RouterModule.forChild(routes);
