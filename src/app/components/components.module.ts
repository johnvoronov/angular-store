import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsRoutes } from './components.routing';
import { ComponentsComponent } from './components.component';
import {ToppingsModalModule} from '@shared/layouts/modal/toppings/toppings-modal.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ComponentsRoutes,
    ToppingsModalModule
  ],
  declarations: [ ComponentsComponent ]
})
export class ComponentsModule { }