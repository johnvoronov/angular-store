import {Component, Inject, OnInit} from '@angular/core'
import {PointService} from '../models/point.service'
import {AppStorage} from '@shared/for-storage/universal.inject'
import {CityService} from '../models/city.service'
import {Point} from '../models/point.model'
import {WrapperComponent} from '@shared/layouts/wrapper/wrapper.component';
import { environment } from '../../environments/environment'

declare var UIkit: any

@Component({
  moduleId: module.id,
  selector: 'app-contacts',
  templateUrl: './contacts.component.html'
})

export class ContactsComponent implements OnInit {
  public city: string
  public points: Point[]
  public latitude: number = 68.976593
  public longitude: number = 33.108485
  public error: string
  public styles = [
    {
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#f5f5f5"
        }
      ]
    },
    {
      "elementType": "labels.icon",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#616161"
        }
      ]
    },
    {
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#f5f5f5"
        }
      ]
    },
    {
      "featureType": "administrative.land_parcel",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#bdbdbd"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#eeeeee"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#e5e5e5"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#9e9e9e"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#ffffff"
        }
      ]
    },
    {
      "featureType": "road.arterial",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#dadada"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#616161"
        }
      ]
    },
    {
      "featureType": "road.local",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#9e9e9e"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#e5e5e5"
        }
      ]
    },
    {
      "featureType": "transit.station",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#eeeeee"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#c9c9c9"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#9e9e9e"
        }
      ]
    }
  ]



  constructor(private cityService: CityService,
              private pointService: PointService,
              @Inject(AppStorage) private appStorage: Storage,
              @Inject(WrapperComponent) private wrapperComponent: WrapperComponent) {
    this.city = this.appStorage.getItem('city')
    this.wrapperComponent.isRootRoute = false
    this.wrapperComponent.isMap = true
  }

  ngOnInit() {
    if (this.city === null || this.city === '') {
      this.getCityByName(environment.defaultCity)
    } else {
      this.getCityByName(this.city)
    }

    UIkit.util.on('#agm-slider', 'itemshown', (e) => {
      let index = e.srcElement.dataset.index
      this.latitude = this.points[index].location.coordinates[0]-0.0050
      this.longitude = this.points[index].location.coordinates[1]
    })
  }

  getCityByName(name: string) {
    this.cityService.getCityByName(name)
      .subscribe(data => {
        this.getPointsByCity(data['response']['_id'])
      }, error => {
        this.error = error;
      })
  }

  getPointsByCity(cityId: string) {
    this.pointService.getPointsByCity(cityId)
      .subscribe(data => {
        this.points = data['response']
      }, error => {
        this.error = error;
      })
  }

  showPoint(index) {
    UIkit.slider('#agm-slider').show(index)
    this.latitude = this.points[index].location.coordinates[0]
    this.longitude = this.points[index].location.coordinates[1]
  }
}