import { ContactsComponent } from './contacts.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '', component: ContactsComponent,
    data: {
      meta: {
        title: 'Контакты',
        override: true,
        description: 'Контакты'
      }
    },
  },
];

export const ContactsRoutes = RouterModule.forChild(routes);
