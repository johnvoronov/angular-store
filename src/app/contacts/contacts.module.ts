import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ContactsComponent } from './contacts.component'
import { ContactsRoutes } from '../contacts/contacts.routing';
 import { AgmCoreModule } from '@agm/core';

@NgModule({
  imports: [
    CommonModule,
    ContactsRoutes,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC--FsBSkrjKSjEgtfOimns3uSzjKoUz-c'
    })
  ],
  declarations: [ContactsComponent],
})
export class ContactsModule { }
