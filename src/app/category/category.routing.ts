import { CategoryComponent } from './category.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '', component: CategoryComponent,
    data: {
      meta: {
        title: 'Название категории',
        override: true,
        description: 'Описание категории'
      }
    },
  },
];

export const CategoryRoutes = RouterModule.forChild(routes);
