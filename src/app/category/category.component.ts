import { Component, OnInit, Inject } from '@angular/core'

import { ActivatedRoute } from '@angular/router'

import { CategoryService } from '../models/category.service'
import { ProductService } from '../models/product.service'
import { CartService } from '../models/cart.service'
import { Category } from '../models/category.model'
import { Product } from '../models/product.model'

import {WrapperComponent} from '@shared/layouts/wrapper/wrapper.component';

import { environment } from '../../environments/environment'
import {MetaService} from '@ngx-meta/core';

import * as $ from 'jquery'

@Component({
  moduleId: module.id,
  selector: 'app-category',
  templateUrl: './category.component.html'
})
export class CategoryComponent implements OnInit {
  public category: Category
  public categoryId: string
  public products: Product[]
  public popularProducts: Product[]
  public defaultImage: string
  public apiUrl: string
  public error: string

  constructor(private readonly meta: MetaService,
              private categoryService: CategoryService,
              private productService: ProductService,
              private cartService: CartService,
              private route: ActivatedRoute,
              @Inject(WrapperComponent) private wrapperComponent: WrapperComponent) {
    this.route.params.subscribe(res => this.categoryId = res.category_id)
    this.wrapperComponent.isRootRoute = false
    this.wrapperComponent.isMap = false
    this.defaultImage = environment.defaultImage
    this.apiUrl = environment.apiUrl
  }

  ngOnInit() {
    this.getCategory(this.categoryId)
    this.getProductsByCategory(this.categoryId)
    this.getPopularProducts()
  }

  getCategory(categoryId: string) {
    this.categoryService.getCategory(categoryId)
      .subscribe(data => {
        this.category = data['response']

        this.meta.setTitle(data['response'].title)
        this.meta.setTag('description', data['response'].title)
        this.meta.setTag('og:title', data['response'].title)
        this.meta.setTag('og:description', data['response'].title)
        this.meta.setTag('og:image', this.apiUrl + data['response'].images[7].src)
      }, error => {
        this.error = error;
      })
  }

  getProductsByCategory(categoryId: string) {
    this.productService.getProductsByCategory(categoryId)
      .subscribe(data => {
        this.products = data['response']
      }, error => {
        this.error = error;
      })
  }

  getPopularProducts() {
    this.productService.getPopularProducts()
      .subscribe(data => {
        this.popularProducts = data['response']
      }, error => {
        this.error = error;
      })
  }

  addProductToCart(product: Product) {
    this.cartService.addProductToCart(product)
      .subscribe(data => {
        // this.wrapperComponent.notify(this.apiUrl + product.images[1].src, '<b>' + product.title + '</b> успешно добавлен(а) в корзину')
        this.calculateCart()
      }, error => {
        this.error = error;
      })
  }

  calculateCart() {
    this.cartService.calculateCart()
      .subscribe(data => {
        const header = $('#cart')
        if (header.hasClass('allow')) {
          header.removeClass('bounce-in-down')
          header.addClass('bounce')
          setTimeout(() => {
            header.removeClass('bounce')
          }, 500)
        } else {
          header.addClass('allow')
        }

        this.wrapperComponent.totalCart = data['response']
      }, error => {
        this.error = error;
      })
  }
}
