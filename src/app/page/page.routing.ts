import { PageComponent } from './page.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '', component: PageComponent,
    data: {
      meta: {
        title: 'Страница',
        override: true,
        description: 'Страница'
      }
    },
  },
];

export const PageRoutes = RouterModule.forChild(routes);
