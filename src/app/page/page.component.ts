import {Component, Inject, OnChanges, OnInit} from '@angular/core';
import {PageService} from '../models/page.service';
import {ActivatedRoute, Params} from '@angular/router';
import {Page} from '../models/page.model';
import {WrapperComponent} from '@shared/layouts/wrapper/wrapper.component';
import {MetaService} from '@ngx-meta/core';

import { environment } from '../../environments/environment'

@Component({
  moduleId: module.id,
  selector: 'app-page',
  templateUrl: './page.component.html'
})

export class PageComponent implements OnInit, OnChanges {
  public page: Page
  public pageKey: string
  public defaultImage: string
  public apiUrl: string
  public error: string

  constructor(private readonly meta: MetaService,
              private pageService: PageService,
              private route: ActivatedRoute,
              @Inject(WrapperComponent) private wrapperComponent: WrapperComponent) {
    this.route.params.subscribe(res => this.pageKey = res.page_key)
    this.wrapperComponent.isMap = false
    this.wrapperComponent.isRootRoute = false
    this.defaultImage = environment.defaultImage
    this.apiUrl = environment.apiUrl
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.pageKey = params['page_key'];
      this.getPage(this.pageKey);
    });

  }

  ngOnChanges() {
    console.log(1)
  }

  getPage(pageKey: string) {
    this.pageService.getPage(pageKey)
      .subscribe(data => {
        this.page = data['response']

        this.meta.setTitle(data['response'].title)
        this.meta.setTag('description', data['response'].content)
        this.meta.setTag('og:title', data['response'].title)
        this.meta.setTag('og:description', data['response'].content)
        if (data['response'].images.length > 0) {
          this.meta.setTag('og:image', this.apiUrl + data['response'].images[7].src)
        }
      }, error => {
        this.error = error;
      })
  }
}
