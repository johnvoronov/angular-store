import { Topping } from "./topping.model";
import { Components } from "./components.model";

export class Product {
  constructor (
    public _id?: string,
    public subid?: string,
    public category?: string,
    public components?: Components,
    public toppings?: {
      list: Topping[]
    },
    public params?: {
      weight: number
    },
    public title?: string,
    public description?: string,
    public price?: number,
    public composition?: string,
    public kcal?: string,
    public proteins?: string,
    public carbohydrates?: string,
    public fats?: string,
    public weight?: string,
    public images?: any,
    public emoji?: string,
    public status?: boolean,
    public count?: number) { }
}
