export class Action {
  constructor (
    public _id?: string,
    public title?: string,
    public description?: string,
    public images?: any,
    public emoji?: string,
    public status?: boolean) { }
}
