export class Point {
  constructor (
    public city?: string,
    public name?: string,
    public address?: string,
    public phone?: string,
    public schedule?: {
      open: string,
      close: string,
      start: string,
      end: string,
      closed: boolean
    },
    public location?: {
      type: string,
      coordinates: any
    },
    public status?: boolean) { }
}