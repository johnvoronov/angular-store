export class City {
  constructor (
    public _id?: string,
    public name?: string,
    public location?: {
      type: string,
      coordinates: any
    },
    public status?: boolean) { }
}
