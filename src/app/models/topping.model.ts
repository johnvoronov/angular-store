import {ɵEMPTY_ARRAY} from '@angular/core';

export class Topping {
  constructor (
    public _id?: string,
    public category?: string,
    public title?: string,
    public description?: string,
    public price?: number,
    public composition?: string,
    public kcal?: string,
    public proteins?: string,
    public carbohydrates?: string,
    public fats?: string,
    public weight?: string,
    public images?: any,
    public emoji?: string,
    public status?: boolean,
    public count?: number) { }
}
