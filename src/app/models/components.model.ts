import { Topping } from './topping.model'
import { Product } from './product.model'

export class Components {
  constructor (
    public _id?: string,
    public category?: string,
    public products?: Product[],
    public list?: string,
    public title?: string,
    public description?: string,
    public images?: any,
    public emoji?: string,
    public status?: boolean) { }
}
