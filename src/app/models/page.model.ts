export class Page {
  constructor (
    public _id?: string,
    public key?: string,
    public title?: string,
    public content?: string,
    public icon?: string,
    public images?: any,
    public status?: boolean) { }
}
