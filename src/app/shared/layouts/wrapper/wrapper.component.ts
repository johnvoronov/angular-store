import {Component, Inject, OnChanges, OnInit} from '@angular/core';
import {CartService} from '../../../models/cart.service';
import { Page } from '../../../models/page.model'
import { City } from '../../../models/city.model'
import {Location} from '@angular/common';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {PageService} from '../../../models/page.service';
import {CityService} from '../../../models/city.service';
import {GeolocationService} from '../../../models/geolocation.service';
import {AppStorage} from '@shared/for-storage/universal.inject';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { environment } from '../../../../environments/environment'

import * as $ from 'jquery'
declare var UIkit: any

@Component({
  selector: 'app-wrapper',
  templateUrl: './wrapper.component.html'
})
export class WrapperComponent implements OnInit {
  public pages: Page[]
  public cities: City[]
  public cityForm: FormGroup
  public isRootRoute: boolean
  public isMap: boolean
  public isCityBoxIn: boolean
  public isCityBoxOut: boolean
  public totalCart: number = 0
  public city: string
  public error: string

  constructor(private location: Location,
              private cartService: CartService,
              private pageService: PageService,
              private geolocationService: GeolocationService,
              private cityService: CityService,
              @Inject(AppStorage) private appStorage: Storage) {
    this.calculateCart()
    this.isRootRoute = true
    this.isMap = false
    this.isCityBoxIn = false
    this.isCityBoxOut = false
    this.city = this.appStorage.getItem('city')
  }

  ngOnInit() {
    $('#open-menu-modal').click(() => {
      $('body').toggleClass('view-menu-modal')
    })
    $('.container-bottom-menu a:not(#open-menu-modal)').click(() => {
      $('body').removeClass('view-menu-modal')
    })
    $('#menu-modal *').click(() => {
      $('body').removeClass('view-menu-modal')
    })

    this.getPages()

    if (this.city === null || this.city === '') {
      this.appStorage.setItem('city', environment.defaultCity)
      this.getGeolocation()
    }
  }

  changeRoute() {
    $('body').removeClass('lazy-load-body')
    setTimeout(() => {
      $('body').addClass('lazy-load-body')
    }, 0)
  }

  closeMenuModal() { }

  notify(image: string, message: string) {
    UIkit.notification({
      message: `<div class="notif-content uk-flex uk-flex-middle">` +
      `<div class="notif-img uk-cover-container"><img src="${ image }"></div>` +
      `<div class="notif-text uk-width-expand">${ message }</div></div>`,
      status: 'primary',
      pos: 'top-center',
      timeout: 3000
    });
  }

  getPages() {
    this.pageService.getPages()
      .subscribe(data => {
        this.pages = data['response']
      }, error => {
        this.error = error;
      })
  }

  calculateCart() {
    this.cartService.calculateCart()
      .subscribe(data => {
        this.totalCart = data['response']
      }, error => {
        this.error = error;
      })
  }

  clearCart() {
    this.cartService.clearCart()
      .subscribe(data => {
        this.totalCart = 0
      }, error => {
        this.error = error;
      })
  }

  goBack() {
    this.location.back()
  }

  getState(outlet) { }

  // Get user geolocation
  getGeolocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.getCity(position.coords.latitude, position.coords.longitude, 'ru')
      }, error => {
        this.getCities()
      })
    } else {
      this.getCities()
      console.log('Geolocation is not supported by this browser.')
    }
  }

  // Set city by data form
  setCity() {
    this.appStorage.setItem('city', this.cityForm.get('city').value)
    this.isCityBoxOut = true
  }

  // Get city by google by user coordinates
  getCity(latitude: number, longitude: number, language: string) {
    this.geolocationService.getCity(latitude, longitude, language)
      .subscribe(data => {
        // Get city by name which google api return
        this.getCityByName(data['results'][0]['address_components'][4]['long_name'])
      }, error => {
        this.error = error;
      })
  }

  // Get cities list which delivery available
  getCities() {
    this.cityService.getCities()
      .subscribe(data => {
        this.cities = data['response']
        this.generateCityForm()
        this.isCityBoxIn = true
      }, error => {
        this.error = error;
      })
  }

  // Get city by name
  getCityByName(name: string) {
    this.cityService.getCityByName(name)
      .subscribe(data => {
        if (data['response'] === null) {
          this.getCities()
        } else {
          this.appStorage.setItem('city', data['response'])
        }
      }, error => {
        this.error = error;
      })
  }

  // Generate city form
  generateCityForm() {
    this.cityForm = new FormGroup({
      city: new FormControl(this.cities[0]['name'], [
        Validators.required
      ])
    })
  }
}
