import {Component, Inject, OnDestroy } from '@angular/core';
import { ToppingService } from '../../../../models/topping.service'
import { CartService } from '../../../../models/cart.service'
import { Product } from '../../../../models/product.model'
import { Topping } from '../../../../models/topping.model'
import { environment } from '../../../../../environments/environment'
import {ProductComponent} from '../../../../product/product.component';
import {WrapperComponent} from '@shared/layouts/wrapper/wrapper.component';

@Component({
  moduleId: module.id,
  selector: 'app-toppings-modal',
  templateUrl: './toppings-modal.component.html',
  providers: [ ProductComponent ]
})

export class ToppingsModalComponent implements OnDestroy {
  public toppings: Topping[]
  public productId: string
  public totalCart: number
  public cart: Product[]
  public defaultImage: string
  public apiUrl: string
  public error: string

  constructor(private toppingService: ToppingService,
              private cartService: CartService,
              @Inject(WrapperComponent) private wrapperComponent: WrapperComponent,
              @Inject(ProductComponent) private productComponent: ProductComponent) {
    this.defaultImage = environment.defaultImage
    this.apiUrl = environment.apiUrl
    this.productId = this.productComponent.productId
    this.getCart()
  }

  ngOnDestroy() {
    let modal = document.getElementById('item-modal-id')
    if (modal !== null) {
      modal.remove()
    }
  }

  getCart() {
    this.cartService.getCart()
      .subscribe(data => {
        this.cart = data['response']
        if (this.productId !== undefined) {
          this.getToppings(this.productId)
        }
      }, error => {
        this.error = error;
      })
  }

  getToppings(productId: string) {
    this.toppingService.getToppingsByProduct(productId)
      .subscribe(data => {
        this.toppings = data['response']

        if (this.toppings.length > 0) {
          this.toppings.map(topping => {
            topping.count = 0
          })

          if (this.cart !== null && this.cart !== undefined && this.cart.length > 0) {
            this.cart.map(async product => {
              if (product._id === productId) {
                if (product.toppings.list.length > 0) {
                  await product.toppings.list.map(async topping => {
                    await this.toppings.map(item => {
                      if (item._id === topping._id) {
                        return item.count = topping.count
                      }
                    })
                  })
                }
              }
            })
          }
        }
      }, error => {
        this.error = error;
      })
  }

  addToppingToProduct(topping: Topping, productId) {
    this.cartService.addToppingToProduct(topping, productId)
      .subscribe(data => {
        this.toppings.map(item => {
          if (item._id === topping._id) {
            if (item.count !== undefined) {
              item.count += 1
            } else {
              item.count = 0
            }
          }
        })
        this.calculateCart()
      }, error => {
        this.error = error;
      })
  }

  deleteToppingFromProduct(productId: string, toppingId: string) {
    this.cartService.deleteToppingFromProduct(productId, toppingId)
      .subscribe(data => {
        this.toppings.map(item => {
          if (item._id == toppingId) {
            item.count -= 1
          }
        })
        this.calculateCart()
      }, error => {
        this.error = error;
      })
  }

  deleteToppingFullFromProduct(productId: string, toppingId: string) {
    this.cartService.deleteToppingFullFromProduct(productId, toppingId)
      .subscribe(data => {
        this.toppings.map(item => {
          if (item._id == toppingId) {
            item.count = 0
          }
        })
        this.calculateCart()
      }, error => {
        this.error = error;
      })
  }

  calculateCart() {
    this.cartService.calculateCart()
      .subscribe(data => {
        this.wrapperComponent.totalCart = data['response']
      }, error => {
        this.error = error;
      })
  }
}


