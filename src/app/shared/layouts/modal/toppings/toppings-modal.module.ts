import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ToppingsModalComponent } from './toppings-modal.component';


@NgModule({
  imports: [CommonModule],
  declarations: [ToppingsModalComponent],
  exports: [ToppingsModalComponent]
})
export class ToppingsModalModule { }
