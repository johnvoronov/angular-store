import {Component, Inject, OnInit} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {WrapperComponent} from '@shared/layouts/wrapper/wrapper.component';
import {Router} from '@angular/router';
import {PointService} from '../models/point.service';
import {CityService} from '../models/city.service';
import {AppStorage} from '@shared/for-storage/universal.inject';
import {environment} from '../../environments/environment';
import {Point} from '../models/point.model';

@Component({
  moduleId: module.id,
  selector: 'app-order',
  templateUrl: './order.component.html'
})

export class OrderComponent implements OnInit {
  public orderForm: FormGroup
  public paymentForm: FormGroup
  public pickupForm: FormGroup
  public deliveryType: string[] = [
    'Доставка',
    'Самовывоз',
    'Экспресс-заказ'
  ]
  public deliveryTime: string[] = [
    'Немедленно',
    'Как можно скорее'
  ]
  public isDeliveryOrder: boolean
  public isExpressOrder: boolean
  public isPickupOrder: boolean
  public isFormValid: boolean
  public isCheckoutSuccess: boolean
  public disabled: string
  public city: string
  public points: Point[]
  public error: string

  constructor(private router: Router,
              private cityService: CityService,
              private pointService: PointService,
              @Inject(AppStorage) private appStorage: Storage,
              @Inject(WrapperComponent) private wrapperComponent: WrapperComponent) {
    this.wrapperComponent.isRootRoute = false
    this.wrapperComponent.isMap = false
    this.isDeliveryOrder = true
    this.isPickupOrder = false
    this.isExpressOrder = false
    this.isFormValid = false
    this.isCheckoutSuccess = false
    this.city = this.appStorage.getItem('city')
  }

  ngOnInit() {
    this.orderForm = new FormGroup({
      name: new FormControl('', [
        Validators.required
      ]),
      phone: new FormControl('', [
        Validators.required,
        Validators.pattern('^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{5,10}$')
      ]),
      deliveryTime: new FormControl(this.deliveryTime[0], [
        Validators.required
      ]),
      deliveryType: new FormControl(this.deliveryType[0], [
        Validators.required
      ]),
      street: new FormControl('', [
        Validators.required
      ]),
      house: new FormControl('', [
        Validators.required
      ]),
      apartment: new FormControl('', [
        Validators.required
      ]),
      entrance: new FormControl(),
      door: new FormControl(),
      floor: new FormControl()
    })

    this.pickupForm = new FormGroup({
      pickupPoint: new FormControl('0', [
        Validators.required
      ])
    })

    this.paymentForm = new FormGroup({
      paymentMethod: new FormControl('', [
        Validators.required
      ]),
      change: new FormControl(''),
    })

    this.onChanges()

    if (this.city === null || this.city === '') {
      this.getCityByName(environment.defaultCity)
    } else {
      this.getCityByName(this.city)
    }
  }

  getCityByName(name: string) {
    this.cityService.getCityByName(name)
      .subscribe(data => {
        this.getPointsByCity(data['response']['_id'])
      }, error => {
        this.error = error;
      })
  }

  getPointsByCity(cityId: string) {
    this.pointService.getPointsByCity(cityId)
      .subscribe(data => {
        this.points = data['response']
      }, error => {
        this.error = error;
      })
  }

  switchOrderType(delivery, pickup, express) {
    this.isDeliveryOrder = delivery
    this.isPickupOrder = pickup
    this.isExpressOrder = express
  }

  onChanges(): void {
    // Change order type
    this.orderForm.get('deliveryType').valueChanges.subscribe(value => {
      if (value === this.deliveryType[0]) {
        this.switchOrderType(true, false, false)
      } else if (value === this.deliveryType[1]) {
        this.switchOrderType(false, true, false)
      } else if (value === this.deliveryType[2]) {
        this.switchOrderType(false, false, true)
      }
    })
  }

  checkoutOrder() {
    if (this.wrapperComponent.totalCart !== 0) {
      this.wrapperComponent.clearCart()
      this.isCheckoutSuccess = true
    } else {
      this.router.navigate(['home'])
    }
  }
}
