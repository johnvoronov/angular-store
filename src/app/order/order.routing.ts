import { OrderComponent } from './order.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '', component: OrderComponent,
    data: {
      meta: {
        title: 'Оформление заказа',
        override: true,
        description: 'Оформление заказа'
      }
    },
  },
];

export const OrderRoutes = RouterModule.forChild(routes);
