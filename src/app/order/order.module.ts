import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { OrderComponent } from './order.component'
import { OrderRoutes } from './order.routing';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    OrderRoutes
  ],
  declarations: [OrderComponent],
})
export class OrderModule { }
