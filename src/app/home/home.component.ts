import {Component, Inject, OnInit} from '@angular/core';

import { CategoryService } from '../models/category.service'
import { ProductService } from '../models/product.service'
import { ActionsService } from '../models/actions.service'
import { Category } from '../models/category.model'
import { Product } from '../models/product.model'
import { Action } from '../models/action.model'

import { environment } from '../../environments/environment'
import {WrapperComponent} from '@shared/layouts/wrapper/wrapper.component';

@Component({
  moduleId: module.id,
  selector: 'app-home',
  templateUrl: './home.component.html',
  providers: [ CategoryService, ProductService, ActionsService ]
})
export class HomeComponent implements OnInit {
  public categories: Category[]
  public products: Product[]
  public actions: Action[]
  public defaultImage: string
  public apiUrl: string
  public error
  public actionsImages: any
  public categoriesImages: any
  public productsImages: any

  constructor(private categoryService: CategoryService,
              private productService: ProductService,
              private actionService: ActionsService,
              @Inject(WrapperComponent) private wrapperComponent: WrapperComponent) {
    this.wrapperComponent.isRootRoute = true
    this.wrapperComponent.isMap = false
    this.defaultImage = environment.defaultImage
    this.apiUrl = environment.apiUrl
  }

  ngOnInit() {
    this.getActions()
    this.getCategories()
    this.getPopularProducts()
  }
  getActions() {
    this.actionService.getActions()
      .subscribe(data => {
        this.actions = data['response']
        this.actionsImages = []

        data['response'].map(action => {
          let imagesString = ''
          let count = 0
          action.images.map(image => {
            if (image.ext === 'jpeg') {
              count++
              if (count !== 4) {
                imagesString += this.apiUrl + image.src + ' ' + image.width + 'w, '
              } else {
                imagesString += this.apiUrl + image.src + ' ' + image.width + 'w'
              }
            }
          })
          this.actionsImages.push(imagesString)
        })
      }, error => {
        this.error = error
      })
  }

  getCategories() {
    this.categoryService.getCategories()
      .subscribe(data => {
        this.categories = data['response']
        this.categoriesImages = []

        data['response'].map(category => {
          let imagesString = ''
          let count = 0
          category.images.map(image => {
            if (image.ext === 'jpeg') {
              count++
              if (count !== 4) {
                imagesString += this.apiUrl + image.src + ' ' + image.width + 'w, '
              } else {
                imagesString += this.apiUrl + image.src + ' ' + image.width + 'w'
              }
            }
          })
          this.categoriesImages.push(imagesString)
        })
      }, error => {
        this.error = error
      })
  }

  getPopularProducts() {
    this.productService.getPopularProducts()
      .subscribe(data => {
        this.products = data['response']
        this.productsImages = []

        data['response'].map(product => {
          let imagesString = ''
          let count = 0
          product.images.map(image => {
            if (image.ext === 'jpeg') {
              count++
              if (count !== 4) {
                imagesString += this.apiUrl + image.src + ' ' + image.width + 'w, '
              } else {
                imagesString += this.apiUrl + image.src + ' ' + image.width + 'w'
              }
            }
          })
          this.productsImages.push(imagesString)
        })
      }, error => {
        this.error = error;
      })
  }
}
