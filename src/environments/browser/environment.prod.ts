export const environment = {
  production: true,
  isServer: false,
  // for prerender
  host: 'http://localhost:18000',
  apiUrl: 'https://telegramopennetwork.pro/api/v1',
  googleMapAPIKey: 'AIzaSyC--FsBSkrjKSjEgtfOimns3uSzjKoUz-c',
  defaultImage: './assets/images/backgrounds/default-w.svg',
  defaultCity: 'Санкт-Петербург'
};
